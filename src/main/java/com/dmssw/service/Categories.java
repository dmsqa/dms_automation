package com.dmssw.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;


import com.dmssw.model.Category;

public class Categories {
	private final String USER_AGENT = "Mozilla/5.0";
	
	public List<Category> GetCategoriesList(String catResponse) throws IOException{
		
		JSONObject obj = new JSONObject(catResponse);
		  List<Category> categories = new ArrayList<Category>();
		 
		  JSONArray array = obj.getJSONArray("data");
		  
		  for(int i = 0 ; i < array.length() ; i++){
			  Category cat = new Category();
			  cat.cateId=array.getJSONObject(i).getInt("cateId");
			  cat.CateName=array.getJSONObject(i).getString("cateName");
			  categories.add(cat);
		  }
		return categories;
		
		
	}
	public String GetCategories() throws IOException {
		  String url = "http://localhost:9090/SingerSFAS-1.0/services/Categories/getCategories";
		  URL obj = new URL(url);
		  HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		  con.setRequestMethod("GET");
		  con.setRequestProperty("User-Agent", USER_AGENT);
		  int responseCode = con.getResponseCode();
		  BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
          StringBuilder sb = new StringBuilder();
          String line;
          while ((line = br.readLine()) != null) {
              sb.append(line+"\n");
          }
          br.close();
          System.out.println("\nSending 'GET' request to URL : " + url);
		  System.out.println("Response Code : " + responseCode);
		  
		  
          return sb.toString();
          
          
		 
	  }
}
