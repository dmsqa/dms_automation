package com.dmssw.model;

public class Category {
public int cateId;
public String CateName;
public String cateDesc;
public int cateStatus;
public String user;
public String remarks;

public int getCateId() {
	return cateId;
}
public void setCateId(int cateId) {
	this.cateId = cateId;
}
public String getCateName() {
	return CateName;
}
public void setCateName(String cateName) {
	CateName = cateName;
}
public String getCateDesc() {
	return cateDesc;
}
public void setCateDesc(String cateDesc) {
	this.cateDesc = cateDesc;
}
public int getCateStatus() {
	return cateStatus;
}
public void setCateStatus(int cateStatus) {
	this.cateStatus = cateStatus;
}
public String getUser() {
	return user;
}
public void setUser(String user) {
	this.user = user;
}
public String getRemarks() {
	return remarks;
}
public void setRemarks(String remarks) {
	this.remarks = remarks;
}

}
