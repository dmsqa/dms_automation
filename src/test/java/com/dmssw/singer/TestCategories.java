package com.dmssw.singer;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

import java.io.IOException;
import java.util.List;

import com.dmssw.service.Categories;
import com.dmssw.model.Category;
public class TestCategories {
	

	Categories cat = new Categories();
	
  @Test
  public void testGetCategories() throws IOException {
	  String res = cat.GetCategories();
	  List<Category> categories = cat.GetCategoriesList(res);
	  System.out.println(categories);
	  
	  
	  Assert.assertEquals(categories.size(), 2);
  }
  @BeforeTest
  public void beforeTest() {
	  
	  
  }

  @AfterTest
  public void afterTest() {
  }

}
